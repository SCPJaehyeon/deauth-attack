#include "header/deauth-attack.h"
using namespace std;

int main(int argc, char *argv[])
{
    if(argc < 3 || argc > 4){
        usage(argv[0]);
        return -1;
    }
    char *dev = argv[1];
    string apmac = argv[2];
    string stmac = "";
    if(argc == 4){
        stmac = argv[3];
    }

    deauth(dev, apmac, stmac);
    return 0;
}
