#include "header/deauth-attack.h"
using namespace std;

void usage(char *argv){
    cout << "Usage : " << argv << " [interface] [ap mac] ([station mac])" << endl;
    cout << "Example1) " << argv << " wlan0 00:11:22:33:44:55" << endl;
    cout << "Example2) " << argv << " wlan0 00:11:22:33:44:55 66:77:88:99:AA:BB" << endl;
}
