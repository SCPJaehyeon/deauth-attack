TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lpcap
SOURCES += \
    cpp/deauth-attack.cpp \
    cpp/main.cpp \
    cpp/print-info.cpp \

HEADERS += \
    header/deauth-attack.h
