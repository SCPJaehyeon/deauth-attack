#pragma once
#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pcap/pcap.h>
#define MAC_LEN 6
#define NON 0x00
#define RT_LEN 0x0008
#define RT_PS 0x00000000
#define DAUTH_FC 0xC0
#define DAUTH_RC 0x0007

class Mac{
protected:
    u_char mac[MAC_LEN];
public:
    Mac() {};
    ~Mac() {};
    operator u_char*() const{
        return (u_char*)mac;
    }
};

struct ieee80211_radiotap_header {
    u_int8_t        it_version;     /* set to 0 */
    u_int8_t        it_pad;
    u_int16_t       it_len;         /* entire length */
    u_int32_t       it_present;     /* fields present */
} __attribute__((__packed__));

struct deauth_mac_header{
    u_int16_t       fc;
    u_int16_t       dr;
    Mac             dmac;
    Mac             smac;
    Mac             bssid;
    u_int16_t       seq;
};

struct deauth_header{
    u_int16_t      rc;
};

#pragma pack(push, 1)
struct deauth_packet{
    ieee80211_radiotap_header radiotaph;
    deauth_mac_header deauthmach;
    deauth_header deauthh;
};
#pragma pack(pop)

void usage(char *argv);
int deauth(char *dev, std::string apmac, std::string stmac);

